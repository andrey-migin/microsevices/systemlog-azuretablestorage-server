﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystemLog.Grpc.Models;
using DotNetCoreDecorators;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using MyAzureTableStorage;

namespace SystemLog.AzureTableStorage
{

    public class SystemLogTableEntity : TableEntity
    {
        
        public static string GeneratePartitionKey(DateTime dateTime)
        {
            return dateTime.Year.ToString("0000") + dateTime.Month.ToString("00") + dateTime.Day.ToString("00");
        }

        public static string GenerateRowKey(DateTime dateTime)
        {
            return dateTime.Hour.ToString("00")
                   + dateTime.Minute.ToString("00")
                   + dateTime.Second.ToString("00")
                   + dateTime.Millisecond.ToString("000");
        }
        
        
        public DateTime DateTime { get; set; }
        public string Process { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        
        public int LogLevel { get; set; }

        public static SystemLogTableEntity Create(LogEventModel src)
        {
            
            
            
            var itm = new SystemLogTableEntity
            {
                PartitionKey = GeneratePartitionKey(src.DateTime),
                RowKey = GenerateRowKey(src.DateTime),
                Process = src.Process,
                Message = src.Message,
                StackTrace = src.StackTrace,
                DateTime = src.DateTime,
                LogLevel = (int)src.LogLevel
            };
            
            
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(itm));

            return itm;

        }


    }
    
    public class SystemLogAzureTableStorage 
    {
        private readonly CloudStorageAccount _cloudStorageAccount;

        private readonly  Dictionary<string, IAzureTableStorage<SystemLogTableEntity>> _tableStorages
            = new Dictionary<string, IAzureTableStorage<SystemLogTableEntity>>();


        private IAzureTableStorage<SystemLogTableEntity> GetTableStorage(string tableName)
        {
            tableName = tableName.ToLower();
            
            lock (_tableStorages)
            {
                if (!_tableStorages.ContainsKey(tableName))
                    _tableStorages.Add(tableName, new AzureTableStorage<SystemLogTableEntity>(_cloudStorageAccount, tableName));

                return _tableStorages[tableName];
            }
        }

        public SystemLogAzureTableStorage(CloudStorageAccount cloudStorageAccount)
        {
            _cloudStorageAccount = cloudStorageAccount;
        }

        public async Task<IEnumerable<string>> GetComponentsAsync()
        {
            var result = new List<string>();

            await foreach (var cloudTable in _cloudStorageAccount.GetCloudTablesAsync())
            {
                if (cloudTable.Name.StartsWith('$'))
                    continue;
                
                result.Add(cloudTable.Name);
            }

            return result;
        }

        public async Task SaveAsync(string component, IEnumerable<LogEventModel> events)
        {

            var tableStorage = GetTableStorage(component);

            var entities = events
                .Select(SystemLogTableEntity.Create)
                .AsReadOnlyList();


            try
            {
                foreach (var entity in entities)
                {
                    await tableStorage.InsertAsync(entity);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }


        public async IAsyncEnumerable<SystemLogTableEntity> GetAsync(string component, DateTime from, DateTime to)
        {

            var dateFrom = from.Date;

            var dateTo = to.Date;

            var tableStorage = GetTableStorage(component);

            while (dateFrom <= dateTo)
            {
                var partitionKey = SystemLogTableEntity.GeneratePartitionKey(dateFrom);

                await foreach (var entity in tableStorage.GetAsync(partitionKey))
                    yield return entity;

                dateFrom = dateFrom.AddDays(1);
            }

        }

    }
}