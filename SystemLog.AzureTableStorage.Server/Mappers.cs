using SystemLog.Grpc.Models;

namespace SystemLog.AzureTableStorage.Server
{
    public static class Mappers
    {

        public static LogEventModel ToGrpcModel(this SystemLogTableEntity entity, string component)
        {
            return new LogEventModel
            {
                DateTime = entity.DateTime,
                Message = entity.Message,
                Process = entity.Process,
                LogLevel = (LogLevel)entity.LogLevel,
                Component = component,
                StackTrace = entity.StackTrace
            };
        }
        
    }
}