using Microsoft.WindowsAzure.Storage;
using MyAzureTableStorage;
using MyDependencies;

namespace SystemLog.AzureTableStorage.Server
{
    public static class ServicesBinder
    {
        public static void BindAzureServices(this IServiceRegistrator sr, SettingsModel settingsModel)
        {
            var cloudStorageAccount = CloudStorageAccount.Parse(settingsModel.AzureTableStorage);
            sr.Register(new SystemLogAzureTableStorage(cloudStorageAccount));
        }
    }
}