using System.Threading.Tasks;
using SystemLog.AzureTableStorage.Server.Services;
using DotNetCoreDecorators;
using MyDependencies;

namespace SystemLog.AzureTableStorage.Server
{
    public static class ServiceLocator
    {
        
        public static readonly EventsQueue EventsQueue = new EventsQueue();


        private static readonly TaskTimer LogWriterTimer = new TaskTimer(1000);
        
        
        public static  SystemLogAzureTableStorage SystemLogAzureTableStorage { get; private set; }


        public static void Init(IServiceResolver sr)
        {
            SystemLogAzureTableStorage = sr.GetService<SystemLogAzureTableStorage>();
            
            LogWriterTimer.Register("LogWriter", () =>
            {
                var (component, events) = EventsQueue.GetEventsToSave();
                return component == null 
                    ? new ValueTask() 
                    : new ValueTask(SystemLogAzureTableStorage.SaveAsync(component, events));
            });
        }

        public static void Start()
        {
            LogWriterTimer.Start();
        }
    }
}