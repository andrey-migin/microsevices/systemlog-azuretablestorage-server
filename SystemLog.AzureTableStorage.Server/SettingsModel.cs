using MySettingsReader;

namespace SystemLog.AzureTableStorage.Server
{
    [YamlAttributesOnly]
    public class SettingsModel
    {
        [YamlProperty("SystemLog.AzureTableStorage")]
        public string AzureTableStorage { get; set; }
    }
}