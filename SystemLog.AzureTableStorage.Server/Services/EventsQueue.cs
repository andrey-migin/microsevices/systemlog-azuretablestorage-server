using System.Collections.Generic;
using SystemLog.Grpc.Models;

namespace SystemLog.AzureTableStorage.Server.Services
{
    public class EventsQueue
    {
        private readonly Dictionary<string, List<LogEventModel>>  _eventsToSave = new Dictionary<string, List<LogEventModel>>();

        private readonly object _lockObject = new object();

        public void Enqueue(string component, IEnumerable<LogEventModel> events)
        {
            lock (_lockObject)
            {
                if (!_eventsToSave.ContainsKey(component))
                    _eventsToSave.Add(component, new List<LogEventModel>());
                _eventsToSave[component].AddRange(events);
            }
        }


        private string FindNotEmpty()
        {
            foreach (var (component, list) in _eventsToSave)
            {
                if (list.Count > 0)
                    return component;
            }

            return null;
        }

        public (string component, IReadOnlyList<LogEventModel>) GetEventsToSave()
        {
            lock (_lockObject)
            {
                var component = FindNotEmpty();
                if (component == null)
                    return (null, null);

                var result = _eventsToSave[component];
                _eventsToSave.Remove(component);
                return (component, result);
            }
            
        }
        
    }
}