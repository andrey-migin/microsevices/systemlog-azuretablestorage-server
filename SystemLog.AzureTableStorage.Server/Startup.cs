using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyDependencies;
using ProtoBuf.Grpc.Server;

namespace SystemLog.AzureTableStorage.Server
{
    public class Startup
    {
        
        private static readonly MyIoc MyIoc = new MyIoc();
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry();
            
            services.AddControllers();
            
            services.AddCodeFirstGrpc();
            
            var settings = MySettingsReader.SettingsReader.GetSettings<SettingsModel>();
            
            MyIoc.BindAzureServices(settings);
            
            ServiceLocator.Init(MyIoc);
            ServiceLocator.Start();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapControllers();
                    endpoints.MapGrpcService<SystemLogApi>();
                });
        }
    }
}