using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SystemLog.Grpc;
using SystemLog.Grpc.Contracts;
using SystemLog.Grpc.Models;
using DotNetCoreDecorators;

namespace SystemLog.AzureTableStorage.Server
{
    public class SystemLogApi : ISystemLogService
    {
        public ValueTask<LogEventResponse> RegisterAsync(LogEventRequest request)
        {
            Console.WriteLine("Event of component: "+request.Component);

            if (request.Events == null)
                return new ValueTask<LogEventResponse>(new LogEventResponse
                {
                    Component = request.Component
                });
            

            var items = request.Events.AsReadOnlyList();
            if (items.Count == 0)
                return new ValueTask<LogEventResponse>(new LogEventResponse
                {
                    Component = request.Component
                });
                
            ServiceLocator.EventsQueue.Enqueue(request.Component, request.Events);
            
            return new ValueTask<LogEventResponse>(new LogEventResponse
            {
                Component = request.Component
            });
        }

        public async ValueTask<GetLogEventsResponse> GetEventsAsync(GetLogEventsRequest request)
        {
            var components = await ServiceLocator.SystemLogAzureTableStorage.GetComponentsAsync();

            var result = new SortedList<DateTime, LogEventModel>();

            foreach (var component in components)
            {
                await foreach (var entity in ServiceLocator.SystemLogAzureTableStorage.GetAsync(component, request.DateFrom, request.DateTo))
                    result.Add(entity.DateTime, entity.ToGrpcModel(component));
            }

            return new GetLogEventsResponse
            {
                Events = result.Values
            };

        }
    }
    
}